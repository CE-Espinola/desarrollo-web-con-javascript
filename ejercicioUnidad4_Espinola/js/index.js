// Aplicar color de fondo #fcf79f al documento
document.querySelector('body').style.background = '#fcf79f';

// Cambiar el color de todos los párrafos (etiqueta p) a verde (#0ca001)
var list = document.querySelectorAll('p');
for (var p of list) {
  p.style.color = '#0ca001';
}

// Aumenta el tamaño de la fuente a 24px, solo del elemento con id "destacado"
document.getElementById('destacado').style.fontSize = '24px';

// Cambiar la familia tipográfica por Arial a los títulos (etiqueta h2)
var list = document.querySelectorAll('h2');
for (var h2 of list) {
  h2.style.fontFamily = 'Arial';
}

//Crear un vínculo con la referencia "https://www.lipsum.com"
var text = 'Lorem Ipsum';
var link = document.getElementById('link');
link.innerHTML =
  '<a href="https://www.lipsum.com" target="_blank" title="Lorem Ipsum">' +
  text +
  '</a>';
